program WinShaver;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, MainFormU, WinShaverUtils,WinUtil, DirectoryCleaner;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm,MainForm);
  Application.Run;
end.

