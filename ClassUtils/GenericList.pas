{==============================================================
  Description:
  "GenericList.pas" has a generic class of list. This is my
  personal creation of a generic list so don't get confused with
  any other GenericList available out there. I don't know if
  there is any similar implementation yet for FPC. Anyway, use at
  your own risk.

  Author:
  Flakron Shkodra

  Contact:
  <flakron.shkodra@gmail.com>

  Copyright (C) 2010
 ==============================================================
}

unit GenericList;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  { TObjectList }

  generic TObjectList<T> = class(TObject)
    type public TArrayOfItems = array of T;
    var private
    private
      _Count:Integer;
      _Items:TArrayOfItems;
      function GetItems(Index: Integer): T;
      procedure SetItems(Index: Integer; const AValue: T);
    public
      constructor Create;

      procedure Add(Item:T);
      procedure AddRange(ArrayOfItems:TArrayOfItems);
      procedure Remove(Index:Integer);
      function Get(Index:Integer):T;
      procedure Clear;
      function ToArray:TArrayOfItems;

      property Items[Index:Integer]:T read GetItems write SetItems;
      property Count:Integer read _Count;

      destructor Destroy;override;
  end;

implementation

{ TObjectList }

function TObjectList.GetItems(Index: Integer): T;
begin
  if (Index>-1) or (Index <_Count) then
  Result := _Items[Index];
end;

procedure TObjectList.SetItems(Index: Integer; const AValue: T);
begin
  _Items[Index] := AValue;
end;

constructor TObjectList.Create;
begin
  inherited Create;
  _Count:=0;
  SetLength(_Items,_Count);
end;

procedure TObjectList.Add(Item: T);
begin
   Inc(_Count,1);
   SetLength(_Items,_Count);
   _Items[_Count-1] := Item;
end;

procedure TObjectList.AddRange(ArrayOfItems: TArrayOfItems);
var
  I: Integer;
begin
  for I := 0 to Length(ArrayOfItems) - 1 do
  begin
    Add(ArrayOfItems[I]);
  end;
end;

procedure TObjectList.Remove(Index: Integer);
begin
   if (Index<_Count) or (Index>-1) then
   begin
     Move(_Items[Index+1],_Items[Index],(_Count-Index) * SizeOf(_Items));
     FillChar(_Items[_Count-1],SizeOf(T),0);
     Dec(_Count,1);
   end else
   raise Exception.Create('Index out of range');
end;

function TObjectList.Get(Index: Integer): T;
begin
    if (Index>-1) or (Index<_Count) then
      Result := _Items[Index];
end;

procedure TObjectList.Clear;
var
  I: Integer;
begin
   for I:=0 to _Count - 1 do
   Remove(0);
end;

function TObjectList.ToArray: TArrayOfItems;
begin
  Result := _Items;
end;


destructor TObjectList.Destroy;
begin
  Clear;
  inherited Destroy;
end;

end.

