unit WinUtil;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,Windows,FileUtil,StrUtils;

function ExtractIcon(handle:THandle;Application:string; IconNo:Integer):HIcon;
function GetSubDirs(Dir: string): TStringList;
function GetSubDirs(Dir, Filter: string):TStringList;
function FindListItem(const List:TStrings; StartsWith:string):Integer;

implementation

function ExtractIcon(handle: THandle; Application: string; IconNo: Integer
  ): HIcon;
begin
  Result := Windows.ExtractIcon(handle,PChar(Application),IconNo);
end;

function GetSubDirs(Dir: string): TStringList;
var
 f:TSearchRec;
 attrib:LongInt;
 lst:TStringList;
 str:String;
begin

  lst := TStringList.Create;
  attrib := 0;
  attrib := faAnyFile;


  if FindFirst(Dir+'\*',attrib,f)=0 then
  begin
    repeat
      str := dir+f.Name;
      if DirectoryExists(str) then
        if (f.Name<>'.') and (f.Name<>'..') then
          lst.Add(str);
    until FindNext(f)<>0;
    FindClose(f.FindHandle);
  end;
  Result := lst;
end;

function GetSubDirs(Dir, Filter: string):TStringList;
var
 f:TSearchRec;
 attrib:LongInt;
 lst:TStringList;
 str:String;
 fLen:Integer;
begin
  lst := TStringList.Create;
  fLen := Length(Filter);
  attrib := 0;
  attrib := faAnyFile;
  if FindFirst(Dir+'\*',attrib,f)=0 then
  begin
    repeat
      str := dir+f.Name;
      if DirectoryExists(str) then
      if Filter=Copy(f.Name,1,fLen) then
      lst.Add(str);
    until FindNext(f)<>0;
   FindClose(f.FindHandle);
  end;
  Result := lst;
end;

function FindListItem(const List: TStrings; StartsWith: string): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I:=0 to List.Count-1 do
  begin
    if AnsiContainsStr(List[I],StartsWith) then
    begin
      Result := I;
      Break;
    end;
  end;
end;


end.

