unit WinShaverUtils;

{$mode objfpc}{$H+}

interface

uses
  Classes,SysUtils,FileUtil,Graphics,fgl,IniFiles,Forms,WinUtil,
  Registry,GraphType;

type

  { TJunkItem }
  //TBitmap = TFPImageBitmap;

  TJunkItem = class
  private
    _DrawRect:TRect;
    _Application: string;
    _Checked: Boolean;
    _DisplayIcon: TBitmap;
    _PathToClean: string;
    _TotalSize:Integer;
    _TotalCount:Integer;
    _JunkFiles:TStringList;  //assigned on Analyze
    _JunkFolders:TStringList;
    _Analyzed:Boolean;
    _Name:string;
    function GetTotalCount: Integer;
    procedure SetApplication(const AValue: string);
    procedure SetChecked(const AValue: Boolean);
    procedure SetDisplayIcon(const AValue: TBitmap);
    procedure SetPathToClean(const AValue: string);
  public
    constructor Create;
    procedure Analyze;
    procedure Clean;
    property Name:string read _Name write _Name;
    property PathToClean:string read _PathToClean write SetPathToClean;
    property ParentApplication:string read _Application write SetApplication;
    property DisplayIcon:TBitmap read _DisplayIcon write SetDisplayIcon;
    property TotalCount:Integer read GetTotalCount;
    property TotalSize:Integer read _TotalSize;
    property Checked:Boolean read _Checked write SetChecked;
    destructor Destroy; override;
  end;

  TJunkItemList = specialize TFPGObjectList<TJunkItem>;

  { TWinUpdateJunk }

  TWinUpdateJunk = class
  private
    _JunkFiles:TStringList;  //assigned on Analyze
    _JunkFolders:TStringList;
    _Name:string;
    _PathToClean:string;
    _TotalSize:Int64;
    function GetTotalSize: Int64;
    procedure SetPathToClean(const AVAlue:string); //this is like 'Analyze'
  public
    constructor Create;
    procedure Clean;
    property Name:string read _Name write _Name;
    property PathToClean:string read _PathToClean write SetPathToClean;
    property TotalSize:Int64 read GetTotalSize;
    destructor Destroy;
  end;

  TWinUpdateJunkList = specialize TFPGObjectList<TWinUpdateJunk>;

  { TWinShaverObject }

  TWinShaverObject = class
  private
    _WinUpdateJunk: TWinUpdateJunkList;
    _JunkItems: TJunkItemList;
    _SettingsLoaded:Boolean;
    procedure SetJunkItems(const AValue: TJunkItemList);
  public
    constructor Create;
    procedure LoadSettings(Filename:string);
    function GetWinUpdateSize:Integer;
    procedure AnalyzeWinUpdateJunk;
    procedure AnalyzeAll;
    procedure CleanAll;
    property Junk:TJunkItemList read _JunkItems write SetJunkItems;
    //property WinUpdateJunk:TWinUpdateJunkList read _WinUpdateJunk;
    destructor Destroy;override;
  end;

implementation

{$Region 'JunkItem'}

{ TJunkItem }

constructor TJunkItem.Create;
var
  ic:TIcon;
begin
  _DisplayIcon := TBitmap.Create;
  _DisplayIcon.Canvas.FloodFill(0,0,clBlack,fsSurface);
  _DisplayIcon.TransparentMode:=tmAuto;
  _DisplayIcon.TransparentColor:=clBlack;
  _DisplayIcon.Transparent:=True;
  _DisplayIcon.Width := 32;
  _DisplayIcon.Height:= 32;
  ic := TIcon.Create;
  ic.Handle := ExtractIcon(HINSTANCE,ParamStr(0),0);
  _DisplayIcon.Canvas.StretchDraw(Rect(0,0,32,32),ic);
  ic.Free;
  _DrawRect.Left:=0;
  _DrawRect.Top:=0;
  _DrawRect.Right:=32;
  _DrawRect.Bottom:=32;
  _JunkFiles := TStringList.Create;
  _JunkFolders := TStringList.Create;
end;

procedure TJunkItem.Analyze;
var
  lstJunkFiles: TStringList;
  I: Integer;
  fs: TFileStream;
  lstFolders: TStringList;
begin
    _TotalSize:=0;
    _TotalCount:=0;
    lstJunkFiles := FindAllFiles(_PathToClean,'*.*',True);
    _JunkFiles.Clear;
    _JunkFiles.AddStrings(lstJunkFiles);
    lstJunkFiles.Free;
    //add files to be cleaned
    for I:=0 to _JunkFiles.Count - 1 do
    begin
      try
        fs := TFileStream.Create(_JunkFiles[I],fmOpenRead or fmShareDenyNone);
        _TotalSize += (fs.Size div 1024);//kb
        fs.Free;
      except
      end;
      Application.ProcessMessages;
    end;
    //also subdirs,
    lstFolders := GetSubDirs(_PathToClean);
    _JunkFolders.Clear;
    _JunkFolders.AddStrings(lstFolders);
    lstFolders.Free;
end;

procedure TJunkItem.Clean;
var
  I: Integer;
begin
  //Delete files
  for I:= 0 to _JunkFiles.Count -1 do
  begin
    DeleteFile(_JunkFiles[I]);
  end;
  //Delete folders
  for I:= 0 to _JunkFolders.Count - 1 do
  begin
    DeleteDirectory(_JunkFolders[I],False);
  end;

  _Analyzed := False;
end;

destructor TJunkItem.Destroy;
begin
  _JunkFiles.Free;
  _JunkFolders.Free;
  _DisplayIcon.Free;
  inherited Destroy;
end;

procedure TJunkItem.SetApplication(const AValue: string);
var
  ik:TIcon;
begin
  _Application:=AValue;
  if FileExists(_Application) then
  begin
    ik := TIcon.Create;
    try
      ik.Handle := ExtractIcon(HINSTANCE,PChar(_Application),0);
      _DisplayIcon.Canvas.StretchDraw(_DrawRect,ik);
    finally
      ik.Free;
    end;
  end;
end;

function TJunkItem.GetTotalCount: Integer;
begin
  Result := _JunkFiles.Count + _JunkFolders.Count;
end;

procedure TJunkItem.SetChecked(const AValue: Boolean);
begin
  if _Checked=AValue then exit;
  _Checked:=AValue;
end;

procedure TJunkItem.SetDisplayIcon(const AValue: TBitmap);
begin
  if _DisplayIcon=AValue then exit;
  _DisplayIcon:=AValue;
end;

procedure TJunkItem.SetPathToClean(const AValue: string);
begin
  if _PathToClean=AValue then exit;
  _PathToClean:=AValue;
end;

{$Endregion}

{$Region 'WinUpdateJunk'}

{ TWinUpdateJunk }

procedure TWinUpdateJunk.SetPathToClean(const AVAlue: string);
var
  lstFil,lstFold:TStringList;
  fs:TFileStream;
  I: Integer;
  ps: LongInt;
begin
  _PathToClean:= AVAlue;
  try
    _TotalSize:=0;
    _JunkFiles.Clear;
    _JunkFolders.Clear;
    lstFil := FindAllFiles(_PathToClean,'*.*',True);;
    _JunkFiles.AddStrings(lstFil);
    lstFil.Free;

    for I:=0 to _JunkFiles.Count-1 do
    begin
      if FileExists(_JunkFiles[I]) then
      begin
        try
          fs :=TFileStream.Create(_JunkFiles[I],fmOpenRead or fmShareDenyNone);
            _TotalSize := _TotalSize+ (fs.Size div 1024);
          fs.Free;
        finally
        end;
      end;
      Application.ProcessMessages;
    end;

    lstFold := GetSubDirs(_PathToClean);
    _JunkFolders.AddStrings(lstFold);
    lstFold.Free;
    ps := Pos('$NTUNINST',Uppercase(_PathToClean));

    _Name := Copy(_PathToClean,ps,Length(_PathToClean) - ps);

    _Name := StringReplace(_Name,'NtUninstall','',[rfReplaceAll]);
    _Name := StringReplace(_Name,'$','',[rfReplaceAll]);

  except on e:exception do
    ShowException(e,@e);
  end;
end;

function TWinUpdateJunk.GetTotalSize: Int64;
begin
  Result := _TotalSize;
end;

constructor TWinUpdateJunk.Create;
begin
  _TotalSize:=0;
  _JunkFiles := TStringList.Create;
  _JunkFolders := TStringList.Create;
end;

procedure TWinUpdateJunk.Clean;
var
  I: Integer;
  reg:TRegistry;
begin
  //Delete files
  for I:= 0 to _JunkFiles.Count -1 do
  begin
    DeleteFile(_JunkFiles[I]);
  end;
  //Delete folders
  for I:= 0 to _JunkFolders.Count - 1 do
  begin
    DeleteDirectory(_JunkFolders[I],False);
  end;
  //now reg entries (so they won't appear in add/remove)

  //reg := TRegistry.Create;
  //try
  //  reg.RootKey:=HKEY_LOCAL_MACHINE;
  //  try
  //    reg.DeleteKey('Software\Microsoft\Windows\CurrentVersion\Uninstall\'+_Name);
  //  except
  //  end;
  //finally
  //  reg.Free;
  //end;
end;

destructor TWinUpdateJunk.Destroy;
begin
  _JunkFiles.Free;
  _JunkFolders.Free;
  inherited Destroy;
end;

{$Endregion}

{$Region 'WinShaverObject'}


{ TWinShaverObject }

constructor TWinShaverObject.Create;
begin
   inherited Create;
   _JunkItems := TJunkItemList.Create;
   _WinUpdateJunk := TWinUpdateJunkList.Create;
end;

procedure TWinShaverObject.SetJunkItems(const AValue: TJunkItemList);
begin
  if _JunkItems=AValue then exit;
  _JunkItems:=AValue;
end;

procedure TWinShaverObject.LoadSettings(Filename: string);
var
  ini:TIniFile;
  j:TJunkItem;
  lstSections:TStringList;
  I: Integer;
  Username:string;
  HomeDrive:string;
  tmp:string;
  IniLine:string;
begin
  try

    _JunkItems.Clear;
    lstSections := TStringList.Create;

    tmp := GetTempDir;

    Username:= GetEnvironmentVariable('USERNAME');
    HomeDrive := Copy(tmp,1,2);

    ini := TIniFile.Create(Filename);
    ini.ReadSections(lstSections);
    for I:=0 to lstSections.Count-1 do
    begin
      j := TJunkItem.Create;
      j.Name:=lstSections[I];
      IniLine := ini.ReadString(lstSections[I],'Path','');
      IniLine := StringReplace(IniLine,'C:',HomeDrive,[rfReplaceAll]);
      IniLine := StringReplace(IniLine,'USERNAME',Username,[rfReplaceAll]);
      j.PathToClean := IniLine;
      j.ParentApplication:=ini.ReadString(lstSections[I],'Application','');
      _JunkItems.Add(j);
    end;
  finally
    ini.Free;
  end;
end;

function TWinShaverObject.GetWinUpdateSize: Integer;
var
  I: Integer;
begin
  Result := 0;
  for I:=0 to _WinUpdateJunk.Count - 1 do
  Result += _WinUpdateJunk.Items[I].TotalSize;
end;

procedure TWinShaverObject.AnalyzeWinUpdateJunk;
var
  j:TWinUpdateJunk;
  lstFolders:TStringList;
  sysPath:string;
  I: Integer;
  reg:TRegistry;
  lstDel:TStringList;
  ieVersion:string;
  testInt: LongInt;
begin
  sysPath := GetEnvironmentVariable('SystemRoot')+'\';
  lstFolders := GetSubDirs(sysPath,'$NtUninstall');
  for I:=0 to lstFolders.Count-1 do
  begin
    j := TWinUpdateJunk.Create;
    j.PathToClean := lstFolders[I]+'\';
    testInt:=j.TotalSize;
    _WinUpdateJunk.Add(j);
    Application.ProcessMessages;
  end;
  lstFolders.Free;

  lstDel := TStringList.Create;
  lstDel.Delimiter:='.';

  reg := TRegistry.Create;
  try
    reg.RootKey:=HKEY_LOCAL_MACHINE;
    if (reg.OpenKey('Software\Microsoft\Internet Explorer',False)) then
    begin
      lstDel.DelimitedText :=  reg.ReadString('Version');
      if lstDel.Count>0 then
      ieVersion := 'ie'+lstDel[0]+'updates';
    end;
  finally
    reg.Free;
    lstDel.Free;
  end;

  j := TWinUpdateJunk.Create;
  j.Name := 'IExplore Updates';
  j.PathToClean := sysPath+ieVersion;


end;

procedure TWinShaverObject.AnalyzeAll;
var
  I: Integer;
begin
  for I:=0 to _JunkItems.Count-1 do
  begin
    _JunkItems.Items[I].Analyze;
  end;
  //AnalyzeWinUpdateJunk;
end;

procedure TWinShaverObject.CleanAll;
var
  I: Integer;
begin
  for I:=0 to _JunkItems.Count-1 do
  begin
    _JunkItems.Items[I].Clean;
  end;
//
//  for I:=0 to _JunkItems.Count-1 do
//  begin
//    _WinUpdateJunk.Items[I].Clean;
//  end;
end;

destructor TWinShaverObject.Destroy;
begin
  _JunkItems.Free;
  _WinUpdateJunk.Free;
  inherited Destroy;
end;

{$Endregion}

end.

