unit DirectoryCleaner;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,FileUtil,Forms;

type

  TDirectoryCleanerProgress = procedure (Sender:TObject; var Progres:Integer) of object;

  { TDirectoryCleaner }

  TDirectoryCleaner = class
  private
    _DirsToClean:TStringList;
    _TotalSize:Integer;
    _TotalCount:Integer;
    _JunkFiles:TStringList;  //assigned on Analyze
    _JunkFolders:TStringList;
    _OnCleanProgress: TDirectoryCleanerProgress;
    _Analyzed:Boolean;
    procedure CheckAnalysis;
    function GetSubDirs(Dir:string):TStringList;
    function GetTotalCount:Integer;
    procedure SetOnCleanProgress(const AValue: TDirectoryCleanerProgress);
  public
    constructor Create;
    constructor Create(DirsToClean:TStringList);overload;
    procedure Clean;
    procedure Analyze;
    property DirsToClean:TStringList read _DirsToClean write _DirsToClean;
    property OnCleanProgress:TDirectoryCleanerProgress read _OnCleanProgress write SetOnCleanProgress;
    property TotalCount:Integer read GetTotalCount;
    property TotalSize:Integer read _TotalSize;
    destructor Destroy;override;
  end;


implementation

{ TDirectoryCleaner }
{$Region 'Constructor/Destructor'}

constructor TDirectoryCleaner.Create;
begin
  inherited Create;
  _JunkFiles := TStringList.Create;
  _JunkFolders := TStringList.Create;
  _DirsToClean := TStringList.Create;
  _TotalCount := 0;
  _TotalSize := 0;
  _Analyzed := False;
end;

constructor TDirectoryCleaner.Create(DirsToClean:TStringList);
begin
  Create;
  _DirsToClean := DirsToClean;
end;

destructor TDirectoryCleaner.Destroy;
begin
   _JunkFiles.Free;
   _JunkFolders.Free;
   _DirsToClean.Free;
   inherited Destroy;
end;
{$Endregion}

{$Region 'Private Members'}

procedure TDirectoryCleaner.SetOnCleanProgress(const AValue: TDirectoryCleanerProgress);
begin
  if _OnCleanProgress=AValue then exit;
  _OnCleanProgress:=AValue;
end;


{$Endregion}

{$Region 'Public Members'}

procedure TDirectoryCleaner.CheckAnalysis;
begin
  if not _Analyzed then
  raise Exception.Create('Analysis not performed yet');
end;

function TDirectoryCleaner.GetSubDirs(Dir: string):TStringList;
var
 f:TSearchRec;
 attrib:LongInt;
 lst:TStringList;
 str:String;
begin

  lst := TStringList.Create;
  attrib := 0;
  attrib := faAnyFile;

  if FindFirst(Dir+'\*',attrib,f)=0 then
  begin
    repeat
      str := dir+f.Name;
      if DirectoryExists(str) then
        if (f.Name<>'.') and (f.Name<>'..') then
          lst.Add(str);
    until FindNext(f)<>0;
    FindClose(f);
  end;
  Result := lst;
end;

function TDirectoryCleaner.GetTotalCount: Integer;
begin
  Result := _JunkFiles.Count + _JunkFolders.Count;
end;

procedure TDirectoryCleaner.Clean;
var
  I: Integer;
begin
  CheckAnalysis;
  //Delete files
  for I:= 0 to _JunkFiles.Count -1 do
  begin
    DeleteFile(_JunkFiles[I]);
  end;
  //Delete folders
  for I:= 0 to _JunkFolders.Count - 1 do
  begin
    DeleteDirectory(_JunkFolders[I],False);
  end;

  _Analyzed := False;
end;

procedure TDirectoryCleaner.Analyze;
var
 I,J: Integer;
 fs: TFileStream;
 lstJunkFiles:TStringList;
 lstFolders:TStringList;
begin

  if _DirsToClean.Count= 0 then
  raise Exception.Create('No folders marked for cleanup!');

  //For each given folder
  for I:=0 to _DirsToClean.Count - 1 do
  begin
    lstJunkFiles := FindAllFiles(_DirsToClean[I],'*.*',True);
    _JunkFiles.AddStrings(lstJunkFiles);
    lstJunkFiles.Free;
    //add files to be cleaned
    for J:=0 to _JunkFiles.Count - 1 do
    begin
      try
        fs := TFileStream.Create(_JunkFiles[J],fmOpenRead or fmShareDenyNone);
        _TotalSize += (fs.Size div 1024);//kb
        Application.ProcessMessages;
      finally
        fs.Free;
      end;
    end;
    //also subdirs,
    lstFolders := GetSubDirs(_DirsToClean[I]);
    _JunkFolders.AddStrings(lstFolders);
    lstFolders.Free;
    Application.ProcessMessages;
  end;
  _Analyzed:=True;
end;

{$Endregion}

end.

