unit MainFormU;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, CheckLst,WinShaverUtils, LCLType,GraphUtil, ExtCtrls, ComCtrls,
  WinUtil,StrUtils,GraphType;

type

  { TMainForm }

  TMainForm = class(TForm)
    btnAbout: TBitBtn;
    btnAnalyze: TBitBtn;
    btnClean: TBitBtn;
    chkJunkItems: TCheckListBox;
    ilImages: TImageList;
    procedure btnAboutClick(Sender: TObject);
    procedure btnAnalyzeClick(Sender: TObject);
    procedure btnCleanClick(Sender: TObject);
    procedure chkJunkItemsDrawItem(Control: TWinControl; Index: Integer;
      ARect: TRect; State: TOwnerDrawState);
    procedure chkJunkItemsShowHint(Sender: TObject; HintInfo: PHintInfo);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
    Shaver:TWinShaverObject;
    procedure LoadShaver;
    procedure LoadSettings;
  public
    { public declarations }
  end;

const

  ApplicationName = 'WinShaver';
  ApplicationVersion = '1.0';
  CRLF = #13#10;


var
  MainForm: TMainForm;
  bmpChecked,bmpUnchecked:TBitmap; //for selection in CheckListBox
implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);
begin
  bmpChecked := TBitmap.Create;
  bmpUnchecked := TBitmap.Create;

  ilImages.GetBitmap(0,bmpUnchecked);
  ilImages.GetBitmap(1,bmpChecked);
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
  LoadSettings;
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
begin
  //LoadSettings;
end;

procedure TMainForm.LoadShaver;
begin

end;

procedure TMainForm.LoadSettings;
var
  ji: TJunkItem;
  I: Integer;
begin
    Shaver :=TWinShaverObject.Create;
    Shaver.LoadSettings(ChangeFileExt(Application.ExeName, '.ini'));

   ji := TJunkItem.Create;
   ji.Name := 'Temporary Files';
   ji.PathToClean:= GetTempDir;
   ji.ParentApplication:='Explorer.exe';
   Shaver.Junk.Add(ji);

   ji := TJunkItem.Create;
   ji.Name:='Recent';
   ji.PathToClean:=GetEnvironmentVariable('UserProfile')+'\Recent\';
   ji.ParentApplication:='C:\WINDOWS\Explorer.exe';
   Shaver.Junk.Add(ji);

   ji := TJunkItem.Create;
   ji.Name:='Temporary Internet Files';
   ji.PathToClean:=GetEnvironmentVariable('UserProfile')+'\Local Settings\'
     +'Temporary Internet Files\';
   ji.ParentApplication:='C:\WINDOWS\Explorer.exe';
   Shaver.Junk.Add(ji);

//
//   ji := TJunkItem.Create;
//   ji.Name:='Windows Garbage';
//   ji.PathToClean:=GetEnvironmentVariable('WINDIR')+'\Installer\';
//   ji.ParentApplication:='C:\WINDOWS\Explorer.exe';
//   Shaver.Junk.Add(ji);





   for I:=0 to Shaver.Junk.Count - 1 do
   begin
     chkJunkItems.Items.Add(Shaver.Junk.Items[I].Name);
   end;
end;

procedure TMainForm.btnAnalyzeClick(Sender: TObject);
var
  I: Integer;
  WinUpdSize:Integer;
begin

  try
    btnAnalyze.Enabled := False;
    btnClean.Enabled := False;;
    chkJunkItems.Clear;

    Caption:='PLEASE WAIT WHILE SEARCHING...';
    Application.ProcessMessages;
    Shaver.AnalyzeAll;
    Caption := ApplicationName +' '+ApplicationVersion;

    for I:=0 to Shaver.Junk.Count-1 do
    begin
      chkJunkItems.Items.Add
      (
        Shaver.Junk.Items[I].Name+' '+
        IntToStr(Shaver.Junk.Items[I].TotalSize)+' kb'
      );
      chkJunkItems.Checked[chkJunkItems.Count-1]:=True;
    end;

    WinUpdSize := Shaver.GetWinUpdateSize;

    btnClean.Enabled:=True;
  finally
    btnAnalyze.Enabled:=True;
  end;

end;

procedure TMainForm.btnAboutClick(Sender: TObject);
var
  f: TForm;
  img: TImage;
begin
  try
    f := CreateMessageDialog(ApplicationName + ' ' + ApplicationVersion +
      CRLF + '=============================================' + CRLF +
      ' 1.Click "Analyze"' + CRLF +
      ' 2. Wait...' + CRLF+
      ' 2. Click "Clean"' +
      CRLF + '=============================================' + CRLF +
      'Flakron Shkodra 2012', mtInformation, [mbOK]);

    img := TImage.Create(f);
    img.Picture.Assign(Application.Icon);
    img.Left := 10;
    img.Top := 10;
    f.InsertControl(img);
    f.ShowModal;
  finally
    f.Free;
  end;


end;

procedure TMainForm.btnCleanClick(Sender: TObject);
var
  I: Integer;
begin

  for I:=0 to Shaver.Junk.Count-1 do
  begin
    if chkJunkItems.Checked[I] then
    begin
      try
      Shaver.Junk.Items[I].Clean;
      Shaver.Junk.Items[I].Analyze;
      except
      end;
    end;
    chkJunkItems.Items[I] :=

        Shaver.Junk.Items[I].Name+' '+
        IntToStr(Shaver.Junk.Items[I].TotalSize)+' kb'
  end;

  //I := FindListItem(chkJunkItems.Items,'Windows Update');
  //if I>-1 then
  //if chkJunkItems.Checked[I] then
  //begin
  //  for I:=0 to Shaver.WinUpdateJunk.Count - 1 do
  //  begin
  //    Shaver.WinUpdateJunk.Items[I].Clean;
  //  end;
  //end;

  btnClean.Enabled:=False;

end;

procedure TMainForm.chkJunkItemsDrawItem(Control: TWinControl; Index: Integer;
  ARect: TRect; State: TOwnerDrawState);
var
 s:TCheckListBox;
 r:TRect;
 stateBmp:TBitmap;
begin
    s:= (Control as TCheckListBox);
    r := ARect;
    r.Top += 8;
    r.Left := 2;
    r.Right := 16;

    with s.Canvas do
    begin
      if Odd(Index) then
      Brush.Color := clWhite
      else
      Brush.Color := $00F9F9F9;
      FillRect(ARect);

      if s.Checked[Index] then
      begin
        GradientFill(ARect,clSilver,clWhite,gdHorizontal);
        stateBmp := bmpChecked;
        Font.Color := clBlue;
      end else
      begin
        stateBmp := bmpUnchecked;
        Font.Color := clBlack;
      end;

      if (odSelected in State) and (odFocused in State) then
      begin
         GradientFill(ARect,clSilver,clGray,gdVertical);
         Font.Color := $0000CCFF;
      end;
      Draw(r.Left,r.Top,stateBmp);
      r.Top:= ARect.Top;
      r.Left:=32;
      r.Right:=66;
      if not AnsiContainsStr(chkJunkItems.Items[Index],'Windows Update') then
        if Shaver.Junk.Items[Index].DisplayIcon<>nil then
          StretchDraw(r,Shaver.Junk.Items[Index].DisplayIcon);
      Brush.Style := bsClear;
      TextOut(r.Right+10,ARect.Top+5,s.Items[Index]);
    end;

end;

procedure TMainForm.chkJunkItemsShowHint(Sender: TObject; HintInfo: PHintInfo);
begin
  chkJunkItems.Hint := Shaver.Junk.Items[chkJunkItems.ItemIndex].PathToClean;
end;



end.

